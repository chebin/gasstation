import store from '@/store';
import NotFound from '@/pages/notfound/not-found/';

function loginGuard( component ) {
    if( !component )
        throw new Error( 'Invalid Component' )

    return ( routeTo, routeFrom, resolve, reject ) => {
        if( store.getters.isLogin ) {
            resolve( { component } )
        } else {
            store.dispatch( 'showLogin', true );
            let unwatch = store.watch(
                ( state ) => {
                    return state.isLoginShown;
                },
                ( value ) => {
                    unwatch();
                    
                    if( store.getters.isLogin ) {
                        resolve( { component } ); 
                    } else {
                        reject();//resolve( { component: NotFound } )
                    }
                })
        }
    }
}

export {
    loginGuard
}
