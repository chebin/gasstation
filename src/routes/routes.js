import * as Guard from './routeGuard.js';

//main
import MainPage from '@/pages/main/main.vue';

//주유소 관련 페이지
import GassView from '@/pages/gasstation/gass-view.vue';
import GassSearch from '@/pages/gasstation/gass-search.vue';
import GassAround from '@/pages/gasstation/gass-around.vue';
import GassLow from '@/pages/gasstation/gass-low.vue';

//고객센터
import CServiceMain from '@/pages/cservice/cservice-main.vue';
import CServiceFaq from '@/pages/cservice/cservice-faq.vue';
import CServiceQuestion from '@/pages/cservice/cservice-question.vue';
import CServiceTerms from '@/pages/cservice/cservice-terms.vue';

//서비스 안내
import ServiceInfo from '@/pages/serviceinfo/service-info.vue';

//주문하기
import OrderMain from '@/pages/order/order-main.vue';
import OrderGas from '@/pages/order/order-gas.vue';
import OrderGasWash from '@/pages/order/order-gaswash.vue';
import OrderWash from '@/pages/order/order-wash.vue';
import OrderComplete from '@/pages/order/order-complete.vue';

//주유
import RefuelMain from '@/pages/refuel/refuel-main.vue';
import RefuelReserve from '@/pages/refuel/refuel-reserve.vue';
import RefuelFindMachine from '@/pages/refuel/refuel-find-machine.vue';
import RefuelDisplay from '@/pages/refuel/refuel-display.vue';
import RefuelOrderOnRefuel from '@/pages/refuel/refuel-order-on-refuel.vue';

//차계부
import LedgerMain from '@/pages/ledger/ledger-main.vue';
import LedgerList from '@/pages/ledger/ledger-list.vue';
import LedgerView from '@/pages/ledger/ledger-view.vue';
import LedgerAdd from '@/pages/ledger/ledger-add.vue';

//포인트, 쿠폰
import PointList from '@/pages/point/point-list.vue';
import PointView from '@/pages/point/point-view.vue';

//공지사항
import BoardList from '@/pages/board/board-list.vue';
import BoardAdd from '@/pages/board/board-add.vue';
import BoardView from '@/pages/board/board-view.vue';

//이벤트
import EventList from '@/pages/event/event-list.vue';
import EventView from '@/pages/event/event-view.vue';

//알림 메세지
import NoticeList from '@/pages/notice/notice-list.vue';

//my
import Register from '@/pages/auth/register.vue';
import MyMain from '@/pages/my/my-main.vue';
import MyChangeInfo from '@/pages/my/change-info.vue';
import MyChangePw from '@/pages/my/change-pw.vue';
import MyHistoryOrder from '@/pages/my/history-order.vue';
import MyHistoryOrderDetail from '@/pages/my/history-order-detail.vue';
import MyHistoryPoint from '@/pages/my/history-point.vue';

//Page Not Found
import NotFoundPage from '@/pages/notfound/not-found.vue';

//========== Route Definition ==========
export default [
  //---------- main ----------
  {
    path: '/',
    component: MainPage,
  },
  //---------- gas station ----------
  {
    path: '/gass-search/',
    component: GassSearch,
  },
  {
    path: '/gass-around',
    component: GassAround
  },
  {
    path: '/gass-low',
    component: GassLow
  },
  {
    path: '/gass-view/:id',
    component: GassView,
  },
  //---------- order ----------
  {
    path: '/order/',
    component: OrderMain  //async: Guard.loginGuard( OrderMain )
  },
  {
    path: '/order/gas/',
    component: OrderGas
  },
  {
    path: '/order/gaswash/',
    component: OrderGasWash
  },
  {
    path: '/order/wash/',
    component : OrderWash
  },
  {
    path: '/order/complete/',
    component: OrderComplete
  },
  //---------- cservice ----------
  {
    path: '/cservice/',
    component: CServiceMain
  },
  {
    path: '/cservice/faq/',
    component: CServiceFaq
  },
  {
    path: '/cservice/question/',
    component: CServiceQuestion
  },
  {
    path: '/cservice/terms/',
    component: CServiceTerms
  },
  //---------- service info ----------
  {
    path: '/serviceinfo/',
    component: ServiceInfo
  },
  //---------- refuel ----------
  {
    path: '/refuel/',
    component: RefuelMain
  },
  {
    path: '/refuel/reserve/',
    component: RefuelReserve
  },
  {
    path: '/refuel/find-machine/',
    component: RefuelFindMachine
  },
  {
    path: '/refuel/display/',
    component: RefuelDisplay
  },
  {
    path: '/refuel/order/',
    component: RefuelOrderOnRefuel
  },
  //---------- ledger ----------
  {
    path: '/ledger/',
    component: LedgerMain
  },
  {
    path: '/ledger/list/',
    component: LedgerList
  },
  {
    path: '/ledger/view/',
    component: LedgerView
  },
  {
    path: '/ledger/add/',
    component: LedgerAdd
  },
  //---------- point ----------
  {
    path: '/point/',
    component: PointList
  },
  {
    path: '/point/view/',
    component: PointView
  },
  //---------- board ----------
  {
    path: '/board/',
    component: BoardList,
  },
  {
    path: '/board/add',
    component: BoardAdd,
  },
  {
    path: '/board/:id',
    component: BoardView,
  },
  //---------- event ----------
  {
    path: '/event/',
    component: EventList
  },
  {
    path: '/event/view',
    component: EventView
  },
  //---------- user ----------
  {
    path: '/auth/register',
    component: Register
  },
  {
    path: '/notice/list',
    component : NoticeList
  },
  {
    path: '/my/',
    component: MyMain
  },
  {
    path: '/my/change-info/',
    component: MyChangeInfo
  },
  {
    path: '/my/change-pw/',
    component: MyChangePw
  },
  {
    path: '/my/history-order/',
    component: MyHistoryOrder
  },
  {
    path: '/my/history-order-detail/:id',
    component: MyHistoryOrderDetail
  },
  {
    path: '/my/history-point/',
    component: MyHistoryPoint
  },
  //---------- Page Not Found ----------
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];
