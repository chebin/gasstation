import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: null,
    user: null,
    isLogin: false,
    isLoginShown: false, //현재 로그인 페이지가 화면에 보이는지 여부
    location: {
      x: 126.9786567,
      y: 37.566826
    },
    lastGasStationList : {
      time : 0,
      list : []
    }
  },
  getters: {
    isLogin: state => {
      return state.isLogin;
    },
    isLoginShown: state => {
      return state.isLoginShown;
    },
    location: state => {
      return state.location;
    }
  },
  mutations: {
    showLogin( state, isShown ) {
      state.isLoginShown = isShown;
    },
    setAuth(state, auth) {
      state.token = auth.token;
      state.user = auth.user;
      state.isLogin = !!(auth.token);
      state.isLoginShown = !state.isLogin;
    },
    setLocation(state, local) {
      state.location = local;
    }
  },
  actions: {
    showLogin({commit}, isShown ) {
      commit('showLogin', isShown);
    },
    setAuth({commit}, auth) {
      commit('setAuth', auth);
    },
    setLocation({commit}, local) {
      commit('setLocation', local);
    }
  }
});