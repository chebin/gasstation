export const category = [
  { name : '회원', children : [
    { name : '가입 및 탈퇴' }
  ]},
  { name : '결제', children : [
    { name : '주유비 결제' }
  ]}
];

export let faq = {};

faq[category[0].name + ':' + category[0].children[0].name] = [
  { title : '회원 가입 방법을 알려주세요', content : '회원가입 방법은' },
  { title : '회원 탈퇴 방법을 알려주세요', content : '회원탈퇴 방법은 메인 우측 상단, 전체메뉴의 환경설정 메뉴를' }   
];

faq[category[1].name + ':' + category[1].children[0].name] = [
  { title : '결제 수단에는 어떤것들이 있나요?', content : '결제 수단에는 신용카드와 앱카드를 이용할 수 있습니다.' }
];
