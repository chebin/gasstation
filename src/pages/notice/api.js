import Api from '@/api';

export default {
  selectListNoticeBySearchKeyword() {
    //전체 회원 조회 : 개수
    return Api().GET(`/notice/selectListNoticeBySearchKeyword`);
  },
  selectListNoticeBySearchKeywordLimit(searchKeyword, endNum = 10) {
    //해당 회원 조회
    return Api().get('/notice/selectListNoticeBySearchKeywordLimit', {
      params: {
        searchKeyword,
        startNum: 1,
        endNum
      }
    });
  },
  selectOneNoticeById(id) {
    //공지사항 게시판 ID 조회
    return Api().GET('/notice/selectOneNoticeById');
  }
}