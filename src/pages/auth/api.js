import Api from '@/api';

export default {
  listMember(rownum = 10) {
    //전체 회원 조회 : 개수
    return Api().GET('/listMember', {
      params: {
        rownum
      }
    });
  },
  member(mmbCardNo) {
    //해당 회원 조회
    return Api().GET('/member', {
      params: {
        mmbCardNo
      }
    });
  },
  memberLogin() {
    //회원 로그인 정보 확인
    return Api().GET('/memberLogin');
  },
  changPasswordMember() {
    //맴버쉽 로그인 비밀번호 변경
    return Api().GET('/');
  },
  regstMemberWithCard() {
    //회원 등록 멥버쉽 카드번호로만 로그인 : 맵버쉽번호,이름,전화번호 일치 확인
    return Api().POST('/');
  },
  regstMemberWithKakao() {
    //회원 등록 멥버쉽 with kakao : 맵버쉽번호,이름,전화번호 일치 확인
    return Api().POST('/');
  },
  updateBasicMember() {
    //맴버쉽 기본정보 변경
    return Api().POST('/');
  },
  updateMarketingMember() {
    //회원 맵버쉽 마케팅 수신정보 수정
    return Api().POST('/');
  },
  updateMyItem() {
    //나의 관심 상품 수정
    return Api().POST('/');
  },
  updatePushMessage() {
    //PUSH 서비스 수신 여부
    return Api().POST('/');
  },
  updateTelphoneMember() {
    //회원 맵버쉽 전화번호 수정
    return Api().POST('/');
  },
  updateTermMember() {
    //회원 맵버쉽 맴버 약관 수정
    return Api().POST('/');
  },
}