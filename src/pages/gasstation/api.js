import axios from 'axios';

export default {
  searchByName(osnm, area) {
    area = (area == 'all') ? '' : area;
    return axios.get('http://www.opinet.co.kr/api/searchByName.do', {
      params: {
        code: 'F288180504',
        out: 'json',
        osnm,
        area
      }
    })
  },
  aroundAll(x, y) {
    return axios.get('http://www.opinet.co.kr/api/aroundAll.do', {
      params: {
        code: 'F288180504',
        out: 'json',
        x,
        y,
        radius: 5000,
        sort: 1,
        prodcd: 'B027'
      }
    });
  },
  detailByid(id) {
    return axios.get('http://www.opinet.co.kr/api/detailById.do', {
      params: {
        code: 'F288180504',
        out: 'json',
        id
      }
    })
  }
}