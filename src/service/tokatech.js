const tokatech = (x, y, is) => {

  Proj4js.defs['WGS84경위도'] = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
  Proj4js.defs['KATEC'] = 
      '+proj=tmerc +lat_0=38 +lon_0=128 +ellps=bessel ' + 
      '+x_0=400000 +y_0=600000 +k=0.9999 +towgs84=-146.43,507.89,681.46 ' + 
      '+units=m +no_defs';
  
  var wgs84 = new Proj4js.Proj('WGS84경위도');
  var katec = new Proj4js.Proj('KATEC');
  
  var p = new Proj4js.Point(x, y);

  if(is) {
    Proj4js.transform(katec, wgs84, p);
  } else {
    Proj4js.transform(wgs84, katec, p);
  }
  
  return {
    x: p.x,
    y: p.y
  }
}
export default tokatech;