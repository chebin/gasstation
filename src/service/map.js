const tmap = {
  basic(el, x, y) {
    // let marker = new daum.maps.Marker({
    //   position: new daum.maps.LatLng(y, x)
    // });

    // let map = new daum.maps.Map(el, {
    //   center: new daum.maps.LatLng(y, x),
    //   level: 3
    // });
    
    //marker.setMap(map);
    var map = new Tmap.Map({
      div: el, // map을 표시해줄 div
      width: '100%',  // map의 width 설정
      height: '400px' // map의 height 설정
    });

		var markerLayer = new Tmap.Layer.Markers();//마커 레이어 생성
    map.addLayer(markerLayer);//map에 마커 레이어 추가
    map.removeZoomControl();
		   
		var lonlat = new Tmap.LonLat(x, y).transform("EPSG:4326", "EPSG:3857");//좌표 설정
		var size = new Tmap.Size(24, 38);//아이콘 크기 설정
		var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점 설정
		var icon = new Tmap.Icon('http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png', size,  offset);//마커 아이콘 설정
    var marker = new Tmap.Marker(lonlat, icon);//마커 생성
    map.setCenter(lonlat, 16);
		markerLayer.addMarker(marker);
  },
  getAddress(x, y) {
    return new Promise((resolve, reject) => {
      var geocoder = new daum.maps.services.Geocoder();
      geocoder.coord2Address(x, y, (res) => {
        resolve({
          address: res[0].address.address_name
        });
      });
    })
  }
}

export default tmap;