// Import Vue
import Vue from 'vue';
import store from '@/store';

// Import F7
import Framework7 from 'framework7/dist/framework7.esm.bundle.js';
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle.js';
import Framework7Styles from 'framework7/dist/css/framework7.css';

// Import Icons and App Custom Styles
import IconsStyles from '@/css/icons.css';
import AppStyles from '@/css/app.scss';

// Import Routes
import Routes from '@/routes/routes.js';
import App from '@/app';

// plugin
import '@/plugins/validate.js';
import PopupManager from '@/plugins/popupmngr.js';

// Init F7 Vue Plugin
Vue.use(Framework7Vue, Framework7);

//Popup Manager
Vue.use( PopupManager );

// Init App
new Vue({
  el: '#app',
  template: '<app/>',
  store,
  framework7: {
    id: 'io.framework7.testapp',
    name: 'Framework7',
    theme: 'auto',
    routes: Routes
  },
  created() {
    navigator.geolocation.watchPosition((pos) => {
      store.dispatch('setLocation', {
        x: pos.coords.longitude,
        y: pos.coords.latitude
      });
    });
  },
  components: {
    app: App
  }
});