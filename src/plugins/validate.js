import Vue from 'vue';
import VeeValidate from 'vee-validate';
import ko from 'vee-validate/dist/locale/ko';

// Init Vee-validate
const config = {
  locale: 'ko',
  dictionary: {
    ko
  }
}
Vue.use(VeeValidate, config);