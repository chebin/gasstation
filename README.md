# Framework7 + Vue + Firebase + Webpack App

test url https://chaben.github.io

# Folder List

## 1. 주유소 찾기(~/pages/gasstation)
Page Name | File Name
------------ | -------------
주유소 찾기 | gass-search.vue
주유소 목록 | gass-list.vue
주유소 보기 | gass-view.vue
주유소 설정 | gass-setting.vue

## 2. 즐겨찾기(~/pages/favorites)
Page Name | File Name
------------ | -------------
즐겨찾기 목록 | fav-list.vue
즐겨찾기 보기 | fav-view.vue

## 3. 세차포인트 & 세차쿠폰(~/pages/point)
Page Name | File Name
------------ | -------------
포인트 & 쿠폰 목록 | point-list.vue
포인트 & 쿠폰 보기 | point-view.vue

## 4. 로그인(~/pages/auth)
Page Name | File Name
------------ | -------------
로그인 | login.vue
회원가입 | register.vue
아이디 찾기 | id-search.vue

## 5. 마이 페이지(~/pages/my)
Page Name | File Name
------------ | -------------
마이페이지 | mypage.vue
회원정보 변경 | myinfo-change.vue
비밀번호 변경 | pass-change.vue
전화번호 변경 | phone-change.vue

## 6
Page Name | File Name
------------ | -------------